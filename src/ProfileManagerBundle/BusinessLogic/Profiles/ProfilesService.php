<?php
/**
 * Created by PhpStorm.
 * User: naeem
 * Date: 30.03.19
 * Time: 10:15
 */

namespace ProfileManagerBundle\BusinessLogic\Profiles;

use ProfileManagerBundle\Repository\ProfilesRepository;
use ProfileManagerBundle\Entity\Profiles;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Psr\Container\ContainerInterface;

class ProfilesService
{
    /**
     * @ProfilesRepository
     */
    private $profileRepository;

    /**
     * @ValidatorInterface
     */
    private $validator;

    /**
     * @ContainerInterface
     */
    private $container;

    /**
     * ProfilesService constructor.
     * @param ProfilesRepository $pr
     * @param ValidatorInterface $validator
     * @param ContainerInterface $container
     */
    public function __construct(ProfilesRepository $pr, ValidatorInterface $validator, ContainerInterface $container)
    {
        $this->profileRepository = $pr;
        $this->validator = $validator;
        $this->container = $container;
    }

    /**
     * @param array $attributes
     * @return array|null
     */
    public function addProfile(Request $request, array $attributes): ?array
    {
        try {
            $fileName = "";
            $profile = new Profiles();
            $profile->setFirstName($attributes['first_name']);
            $profile->setLastName($attributes['last_name']);
            $profile->setStreet($attributes['street']);
            $profile->setZip($attributes['zip']);
            $profile->setCity($attributes['city']);
            $profile->setCountry($attributes['country']);
            $profile->setPhoneNunber($attributes['phone']);
            $profile->setBirthday(new \DateTime($attributes['birthday']));
            $profile->setEmailAddress($attributes['email']);
            $errors = $this->validator->validate($profile);
            if (count($errors) > 0) {
                $errorsString = (string) $errors;

                return array(
                    'data' => $errorsString,
                    'status' => 403
                );
            }
            if ($request->files->get('profile_picture')) {
                if (!in_array($request->files->get('profile_picture')->guessExtension(), ["jpg", "png", "jpeg"])) {

                    return array(
                        'status' => 403,
                        'data' => "Only jpg or png images allowed"
                    );
                }
                $fileName = uniqid() . "." . $request->files->get('profile_picture')->guessExtension();
                $request->files->get('profile_picture')
                    ->move($this->container->getParameter('images_dir') . "/web/images", $fileName);
            }
            $attributes['picture'] = $fileName;
            $this->profileRepository->addProfile($attributes);

            return array(
                'data' => "Profile added successfully",
                'status' => 201
            );
        } catch (\Exception $ex) {

            return array(
                'data' => $ex->getMessage(),
                'status' => 500
            );
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function removeProfile($id)
    {
        try {
            $profile = $this->profileRepository->find($id);
            $file = $profile->getPictureUrl();
            $this->profileRepository->removeProfile($profile);
            if ($file) {
                unlink($this->container->get('kernel')->getProjectDir() . '/web/images/' . $file);
            }

            return array(
                'status' => 202,
                'data' => "Profile Deleted Successfully"
            );
        } catch (\Exception $ex) {

            return array(
                'status' => 500,
                'data' => $ex->getMessage()
            );
        }
    }

    /**
     * @param Request $request
     * @param array $attributes
     * @return array
     */
    public function editProfile(Request $request, array $attributes)
    {
        try {
            $fileName = "";
            $profile = new Profiles();
            $profile->setFirstName($attributes['first_name']);
            $profile->setLastName($attributes['last_name']);
            $profile->setStreet($attributes['street']);
            $profile->setZip($attributes['zip']);
            $profile->setCity($attributes['city']);
            $profile->setCountry($attributes['country']);
            $profile->setPhoneNunber($attributes['phone']);
            $profile->setBirthday(new \DateTime($attributes['birthday']));
            $profile->setEmailAddress($attributes['email']);
            $errors = $this->validator->validate($profile);
            if (count($errors) > 0) {
                $errorsString = (string) $errors;

                return array(
                    'data' => $errorsString,
                    'status' => 403
                );
            }
            if ($request->files->get('profile_picture')) {
                if (!in_array($request->files->get('profile_picture')->guessExtension(), ["jpg", "png", "jpeg"])) {

                    return array(
                        'status' => 403,
                        'data' => "Only jpg or png images allowed"
                    );
                }
                $fileName = uniqid() . "." . $request->files->get('profile_picture')->guessExtension();
                $request->files->get('profile_picture')
                    ->move($this->container->getParameter('images_dir') . "/web/images", $fileName);
            }
            $attributes['picture'] = $fileName;
            $this->profileRepository->editProfile($attributes);

            return array(
                'data' => "Profile updated successfully",
                'status' => 201
            );
        } catch (\Exception $ex) {

            return array(
                'data' => $ex->getMessage(),
                'status' => 500
            );
        }
    }

    /**
     * @param int $from
     * @param int $to
     * @return mixed
     */
    public function getPaginatedProfiles(int $from, int $to)
    {
        return $this->profileRepository->getProfiles($from, $to);
    }

    /**
     * @return array
     */
    public function getProfiles()
    {
        return $this->profileRepository->findAll();
    }


    /**
     * @param $id
     * @return object|null
     */
    public function getProfileById($id)
    {
        return $this->profileRepository->find($id);
    }

}