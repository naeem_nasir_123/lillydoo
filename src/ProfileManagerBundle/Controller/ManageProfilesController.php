<?php

namespace ProfileManagerBundle\Controller;

use ProfileManagerBundle\BusinessLogic\Profiles\ProfilesService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/")
 */
class ManageProfilesController extends Controller
{

    /**
     * @Route("")
     */
    public function homeAction()
    {
        return $this->render('@LillyDooProfileManager/mainMenu.html.twig');
    }

    /**
     * @Route("manage-profiles")
     */
    public function manageProfilesAction(ProfilesService $profilesService)
    {
        $profiles = $profilesService->getProfiles();
        return $this->render('@LillyDooProfileManager/manageProfiles.html.twig', ['profiles' => $profiles]);
    }

    /**
     * @Route("add-profile")
     */
    public function addProfilesAction()
    {
        return $this->render('@LillyDooProfileManager/addProfile.html.twig');
    }

    /**
     * @Route("add")
     * @Method({"POST"})
     * @param Request $request
     * @param ProfilesService $profiles
     * @return JsonResponse
     */
    public function addProfile(Request $request, ProfilesService $profiles)
    {
        $params = array(
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'street' => $request->get('street'),
            'zip' => $request->get('zip'),
            'city' => $request->get('city'),
            'country' => $request->get('country'),
            'phone' => $request->get('phone'),
            'birthday' => $request->get('birthday'),
            'email' => $request->get('email')
        );
        $result = $profiles->addProfile($request, $params);
        $response = new JsonResponse();
        $response->setData($result['data']);
        $response->setStatusCode($result['status']);

        return $response;
    }

    /**
     * @Route("remove")
     * @Method({"DELETE"})
     * @param Request $request
     * @param ProfilesService $profileService
     * @return JsonResponse
     */
    public function removeProfile(Request $request, ProfilesService $profileService)
    {
        $result = $profileService->removeProfile($request->get('id'));
        $response = new JsonResponse();
        $response->setData($result['data']);
        $response->setStatusCode($result['status']);

        return $response;

    }

    /**
     * @Route("edit-profile")
     * @param Request $request
     * @param ProfilesService $profilesService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editProfileAction(Request $request, ProfilesService $profilesService)
    {
        $profile = $profilesService->getProfileById($request->get('id'));

        return $this->render('@LillyDooProfileManager/editProfile.html.twig', ['profile' => $profile]);
    }

    /**
     * @Route("edit")
     * @Method({"POST"})
     * @param Request $request
     * @param ProfilesService $profilesService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, ProfilesService $profileService)
    {
        $params = array(
            'id' => $request->get('id'),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'street' => $request->get('street'),
            'zip' => $request->get('zip'),
            'city' => $request->get('city'),
            'country' => $request->get('country'),
            'phone' => $request->get('phone'),
            'birthday' => $request->get('birthday'),
            'email' => $request->get('email')
        );
        $result = $profileService->editProfile($request, $params);
        $response = new JsonResponse();
        $response->setStatusCode($result['status']);
        $response->setData($result['data']);

        return $response;
    }

    /**
     * @Route("list-profiles")
     * @Method({"GET"})
     * @param ProfilesService $profileService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listProfileAction(Request $request, ProfilesService $profileService)
    {
        $profiles = $profileService->getPaginatedProfiles($request->get('page', 1), 2);

        return $this->render('@LillyDooProfileManager/listProfiles.html.twig', ['profiles' => $profiles]);

    }
}
