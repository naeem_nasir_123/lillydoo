<?php

namespace ProfileManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Profiles
 *
 * @ORM\Table(name="profiles")
 * @ORM\Entity(repositoryClass="ProfileManagerBundle\Repository\ProfilesRepository")
 */
class Profiles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(name="first_name", type="string", length=500)
     */
    private $firstName;

    /**
     * @var string
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(name="last_name", type="string", length=500)
     */
    private $lastName;

    /**
     * @var string|null
     * @Assert\NotBlank
     * @Assert\Type("string")
     *
     * @ORM\Column(name="street", type="string", length=1000)
     */
    private $street;

    /**
     * @var int|null
     * @Assert\NotBlank
     * @Assert\Type("string")
     *
     * @ORM\Column(name="zip", type="string", length=500)
     */
    private $zip;

    /**
     * @var string|null
     * @Assert\NotBlank
     * @Assert\Type("string")
     *
     * @ORM\Column(name="city", type="string", length=500)
     */
    private $city;

    /**
     * @var string|null
     * @Assert\NotBlank
     * @Assert\Type("string")
     *
     * @ORM\Column(name="country", type="string", length=500)
     */
    private $country;

    /**
     * @var int|null
     * @Assert\NotBlank
     * @Assert\Type("string")
     *
     * @ORM\Column(name="phone_nunber", type="string", length=500)
     */
    private $phoneNunber;

    /**
     * @var \DateTime|null
     * @Assert\NotBlank
     * @Assert\DateTime
     *
     * @ORM\Column(name="birthday", type="datetime")
     */
    private $birthday;

    /**
     * @var string|null
     *
     * @Assert\Email
     * @Assert\NotBlank
     * @ORM\Column(name="email_address", type="string", length=500)
     */
    private $emailAddress;

    /**
     * @var string
     *
     * @Assert\Type("string")
     *
     * @ORM\Column(name="picture_url", type="string", length=1000,  nullable=true)
     */
    private $pictureUrl;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return Profiles
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return Profiles
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set street.
     *
     * @param string|null $street
     *
     * @return Profiles
     */
    public function setStreet($street = null)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set zip.
     *
     * @param int|null $zip
     *
     * @return Profiles
     */
    public function setZip($zip = null)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip.
     *
     * @return int|null
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city.
     *
     * @param string|null $city
     *
     * @return Profiles
     */
    public function setCity($city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country.
     *
     * @param string|null $country
     *
     * @return Profiles
     */
    public function setCountry($country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phoneNunber.
     *
     * @param int|null $phoneNunber
     *
     * @return Profiles
     */
    public function setPhoneNunber($phoneNunber = null)
    {
        $this->phoneNunber = $phoneNunber;

        return $this;
    }

    /**
     * Get phoneNunber.
     *
     * @return int|null
     */
    public function getPhoneNunber()
    {
        return $this->phoneNunber;
    }

    /**
     * Set birthday.
     *
     * @param \DateTime|null $birthday
     *
     * @return Profiles
     */
    public function setBirthday($birthday = null)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday.
     *
     * @return \DateTime|null
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set emailAddress.
     *
     * @param string|null $emailAddress
     *
     * @return Profiles
     */
    public function setEmailAddress($emailAddress = null)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress.
     *
     * @return string|null
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set pictureUrl.
     *
     * @param string $pictureUrl
     *
     * @return Profiles
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Get pictureUrl.
     *
     * @return string
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }
}
